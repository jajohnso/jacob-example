# Welcome

This is a wonderful README to be used for the bootcamp project.

Below is a set of ingredients for a cinnamon roll recipe.

## Dough Ingredients

1 cup milk
1/4 cup (56 grams) unsalted butter
3 1/4 cups (405 grams) all-purpose flour
1/4 cup (50 grams) white granulated sugar
2 1/4 teaspoons (7 grams) instant-rise yeast
2 teaspoons ground cardamom
3/4 teaspoon fine sea salt

## Filling ingredients

1/2 cup (113 grams) unsalted butter
1/2 cup (100 grams) packed light brown sugar
2 tablespoons ground cinnamon
1 teaspoon ground cardamom
1 teaspoon vanilla extract
